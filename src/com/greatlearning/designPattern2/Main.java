package com.greatlearning.designPattern2;

public class Main {
    public static void main(String[] args) {
        System.out.printf("\n ---Creating bank builder object and setting only mandatory fields---\n");
        Bank bank = new Bank.BankBuilder("111",
                "Savings", "Bangalore", 1000).build();
        System.out.println(bank);

        System.out.printf("\n ---Creating bank builder object and setting mandatory/optional fields---\n");
        Bank bankWithOptionalParam = new Bank.BankBuilder("111",
                "Savings", "Bangalore", 1000)
                .setAtmTransactions("1:credit, 2:deposit").setEmiSchedule("3 months").build();
        System.out.println(bankWithOptionalParam);
    }
}
