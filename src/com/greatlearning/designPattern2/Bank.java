package com.greatlearning.designPattern2;

public class Bank {
    // mandatory attributes
    public String bankAccountNo;
    public String accountType;
    public String branch;
    public Integer balance;

    // optional attributes
    public String atmTransactions;
    public String emiSchedule;

    // constructor to access builder static class
    public Bank(BankBuilder bankBuilder) {
        this.bankAccountNo = bankBuilder.bankAccountNo;
        this.accountType = bankBuilder.accountType;
        this.branch = bankBuilder.branch;
        this.balance = bankBuilder.balance;
        this.atmTransactions = bankBuilder.atmTransactions;
        this.emiSchedule = bankBuilder.emiSchedule;
    }

    // Creating static builder class to build the mandatory/optional parameters
    public static class BankBuilder {
        // mandatory attributes
        public String bankAccountNo;
        public String accountType;
        public String branch;
        public Integer balance;

        // optional attributes
        public String atmTransactions;
        public String emiSchedule;

        public BankBuilder(String bankAccountNo, String accountType, String branch, Integer balance) {
            this.bankAccountNo = bankAccountNo;
            this.accountType = accountType;
            this.branch = branch;
            this.balance = balance;
        }

        public BankBuilder setAtmTransactions(String atmTransactions) {
            this.atmTransactions = atmTransactions;
            return this;
        }

        public BankBuilder setEmiSchedule(String emiSchedule) {
            this.emiSchedule = emiSchedule;
            return this;
        }

        // Creating builder method
        public Bank build() {
            return new Bank(this);
        }

    }

    public String getBankAccountNo() {
        return bankAccountNo;
    }

    public void setBankAccountNo(String bankAccountNo) {
        this.bankAccountNo = bankAccountNo;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public String getAtmTransactions() {
        return atmTransactions;
    }

    public void setAtmTransactions(String atmTransactions) {
        this.atmTransactions = atmTransactions;
    }

    public String getEmiSchedule() {
        return emiSchedule;
    }

    public void setEmiSchedule(String emiSchedule) {
        this.emiSchedule = emiSchedule;
    }

    @Override
    public String toString() {
        return "Bank{" +
                "bankAccountNo='" + bankAccountNo + '\'' +
                ", accountType='" + accountType + '\'' +
                ", branch='" + branch + '\'' +
                ", balance=" + balance +
                ", atmTransactions='" + atmTransactions + '\'' +
                ", emiSchedule='" + emiSchedule + '\'' +
                '}';
    }
}
