package com.greatlearning.designPattern3;

public class GBPConverter implements ICurrencyConvertor {

    @Override
    public double convertToINR(double amount) {
        return amount * 100.88;
    }
}
