package com.greatlearning.designPattern3;

public enum ICurrencyConvertorType {
    GBP,
    USD
}
