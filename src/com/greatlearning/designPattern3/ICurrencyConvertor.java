package com.greatlearning.designPattern3;

public interface ICurrencyConvertor {
    // interface to convert the currency amount
    double convertToINR(double amount);
}
