package com.greatlearning.designPattern3;

public class CurrencyConvertorFactory {
    // create method to select the convertor class based on user selection
    public static ICurrencyConvertor getCurrencyConvertor(ICurrencyConvertorType currencyConvertorType) {
        if (currencyConvertorType.equals(ICurrencyConvertorType.GBP)) {
            // if pound selected
            return new GBPConverter();
        } else if (currencyConvertorType.equals(ICurrencyConvertorType.USD)) {
            // if usd selected
            return new DollarConvertor();
        } else {
            return null;
        }
    }
}
