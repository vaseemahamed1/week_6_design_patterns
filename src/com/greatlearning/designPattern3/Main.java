package com.greatlearning.designPattern3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        String menuChoice;
        int amount;

        System.out.printf("---- Please select the Currency convert you would like to use----\n");
        System.out.printf("\n 1. GBS \n 2. USD \n");
        menuChoice = input.nextLine();
        System.out.printf("---- please provide the amount to be converted----\n");
        amount = input.nextInt();

        switch (menuChoice.toLowerCase()) {
            case "gbs":
                Double result = CurrencyConvertorFactory.getCurrencyConvertor(ICurrencyConvertorType.GBP).convertToINR(amount);
                System.out.printf("result: " + result);
                break;
            case "usd":
                Double usdResult = CurrencyConvertorFactory.getCurrencyConvertor(ICurrencyConvertorType.USD).convertToINR(amount);
                System.out.printf("result: " + usdResult);
                break;
        }
    }
}
