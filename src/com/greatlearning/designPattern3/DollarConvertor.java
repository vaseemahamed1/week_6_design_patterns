package com.greatlearning.designPattern3;

public class DollarConvertor implements ICurrencyConvertor {

    @Override
    public double convertToINR(double amount) {
        return amount * 74.42;
    }
}
