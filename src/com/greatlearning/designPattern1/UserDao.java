package com.greatlearning.designPattern1;

import java.sql.SQLException;

public interface UserDao {
    // abstract method to access employee list
    void getEmployees() throws SQLException;
}
