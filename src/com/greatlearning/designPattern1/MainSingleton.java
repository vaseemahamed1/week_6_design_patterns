package com.greatlearning.designPattern1;

import java.sql.SQLException;

public class MainSingleton {
    public static void main(String[] args) throws SQLException {
        // create instance to use dao
        UserDaoImplementation userDAO1 = new UserDaoImplementation();
        // get the list of users from table
        userDAO1.getEmployees();

        // create instance to use dao2 which will use same connection object to access data from db
        UserDaoImplementation userDAO2 = new UserDaoImplementation();
        // get the list of users from table
        userDAO2.getEmployees();
    }
}
