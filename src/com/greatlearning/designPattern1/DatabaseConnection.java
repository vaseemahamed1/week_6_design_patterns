package com.greatlearning.designPattern1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {

    private static Connection con;

    private DatabaseConnection() {
    }

    // static block to create a db connection instance on class loader
    static {
        try {
            con = DriverManager.getConnection("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1", "", "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // method to access the connection object
    public static synchronized Connection getConnection() {
        return con;
    }
}
