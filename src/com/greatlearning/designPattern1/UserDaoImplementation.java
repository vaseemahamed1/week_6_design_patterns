package com.greatlearning.designPattern1;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class UserDaoImplementation implements UserDao {


    static Connection con = DatabaseConnection.getConnection();

    // initial sql code
    Statement statement;

    {
        try {
            statement = con.createStatement();
            statement.execute("DROP TABLE IF EXISTS USERS");
            statement.execute("CREATE TABLE users(userId int primary key, userName varchar(255))");
            statement.execute("INSERT INTO users VALUES (111, 'test1')");
            statement.execute("INSERT INTO users VALUES (122, 'test2')");
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }


    // method implementation to get all users
    @Override
    public void getEmployees() throws SQLException {
        String query = "select userId,userName from users";
        ResultSet results = statement.executeQuery(query);
        System.out.printf("\nUserId " + "UserName \n");
        while (results.next()) {
            System.out.println(results.getInt("userId")
                    + " " + results.getString("userName"));
        }
    }
}
